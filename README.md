## Docker Cheat Sheet

### Simple Guide

###### Author: *pathum bandara*

- This repository includes multiple branches, each with a Docker image for a specific programming language.
   - [docker-with-java](https://gitlab.com/pathum_bandara/docker-images/-/tree/docker-with-java)
   - [docker-with-laravel](https://gitlab.com/pathum_bandara/docker-images/-/tree/docker-with-laravel)
   - [docker-with-php](https://gitlab.com/pathum_bandara/docker-images/-/tree/docker-with-php)
   - [docker-with-python](https://gitlab.com/pathum_bandara/docker-images/-/tree/docker-with-python)
   - [docker-with-ubuntu](https://gitlab.com/pathum_bandara/docker-images/-/tree/docker-with-ubuntu)
- simply you can pull these docker images from dockerhub using `docker pull pathumb/demo:v1` (example)
- https://hub.docker.com/repositories/pathumb
- then run `docker-compose up`  **or** `docker run -p <host_port>:<container_port> <image_name>`
- **Note** Check the port before run it.
- Simple: [Learn More](https://docs.docker.com/get-started/docker_cheatsheet.pdf) or Deep: [Learn More](https://github.com/wsargent/docker-cheat-sheet)

#### Commands

- All docker images : (ls for list)
```
   docker images ls
```
- All running containers : (-a for all)
```
  docker ps -a
```
- build docker image : (.[dot] for current directory)
```
   docker build -t <image_name> .
``` 
- run docker image on *browser* : 
```
   docker run -p <host_port>:<container_port> <image_name>
```
- run docker image on *terminal* : 
```
   docker run <image_name>:<tag_name>
```
- push to dockerhub : 
```
   docker tag your-dockerhub-username/your-image-name:tag-name your-dockerhub-username/your-image-name:new-tag-name
   docker push your-dockerhub-username/your-image-name:new-tag-name
```

## Sample Docker Template
##### Dockerfile:
```
# Base image
FROM python:3.9-slim-buster

# Set the working directory
WORKDIR /app

# Copy the requirements file to the working directory
COPY requirements.txt .

# Install the required packages
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the application code to the working directory
COPY . .

# Expose the port on which the application will run
EXPOSE 5000

# Start the application
CMD ["python", "app.py"]

```

##### docker-compose.yml:

```
version: "3.9"

services:
  app:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/app
    environment:
      - FLASK_ENV=development
```

##### requirements.txt:

```
flask==2.0.1
```
